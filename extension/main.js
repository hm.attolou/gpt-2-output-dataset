let title = document.getElementById('title');
let textbox = document.getElementById('textbox');
let last_submit = null;

let real_percentage = document.getElementById('real-percentage');
let fake_percentage = document.getElementById('fake-percentage');
let bar = document.getElementById('bar');
let message = document.getElementById('message');


function update_graph(result) {
    if (result === null) {
        real_percentage.innerHTML = '';
        fake_percentage.innerHTML = '';
        bar.style.width = '50%';
        message.innerHTML = '';
    } else {
        let percentage = result.real_probability;
        real_percentage.innerHTML = (100 * percentage).toFixed(2) + '%';
        fake_percentage.innerHTML = (100 * (1 - percentage)).toFixed(2) + '%';
        bar.style.width = (100 * percentage).toFixed(2) + '%';
        if (result.used_tokens === result.all_tokens) {
            message.innerHTML = `Prediction based on ${result.used_tokens} tokens`;
        } else {
            message.innerHTML = `Prediction based on the first ${result.used_tokens} tokens among the total ${result.all_tokens}`;
        }
    }
}

function req() {
    let req = new XMLHttpRequest();
    if (textbox.value.length === 0) {
        update_graph(null);
        return;
    }
    req.open('GET', 'http://localhost:8080/?' + textbox.value, true);
    req.onreadystatechange = () => {
        if (req.readyState !== 4) return;
        if (req.status !== 200) throw new Error("HTTP status: " + req.status);
        let result = JSON.parse(req.responseText);
        update_graph(result);
    };
    req.send();
}

textbox.oninput = () => {
    if (last_submit) {
        clearTimeout(last_submit);
    }
    if (textbox.value.length === 0) {
        update_graph(null);
        return;
    }
    message.innerText = 'Predicting ...';
    last_submit = setTimeout(() => {
        req();
    }, 1000);

};

window.addEventListener('DOMContentLoaded', async () => {
    textbox.focus();
    const [tab] = await chrome.tabs.query({active: true, currentWindow: true});
    let result;
    try {
        [{result}] = await chrome.scripting.executeScript({
            target: {tabId: tab.id},
            function: () => getSelection().toString(),
        });
        textbox.innerHTML = result
        req()
    } catch (e) {
        {
        }// ignoring an unsupported page like chrome://extensions
    }
});